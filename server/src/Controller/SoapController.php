<?php

namespace App\Controller;

use App\Service\AuthorService;
use App\Service\BookService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class SoapController extends AbstractController
{
    /**
     * @Route("/ws/{type}")
     */
    public function server(BookService $bookService, AuthorService $authorService, $type)
    {
        ini_set("soap.wsdl_cache_enabled", "0");
        if($type == 'book') {
            $object = $bookService;
        } elseif ($type == 'author') {
            $object = $authorService;
        } else {
            return $this->createNotFoundException();
        }
        $soapServer = new \SoapServer("/var/www/html/tp-soap/server/public/$type.wsdl");
        $soapServer->setObject($object);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

        ob_start();
        $soapServer->handle();
        $response->setContent(ob_get_clean());

        return $response;
    }
}