<?php

namespace App\Service;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\ORM\EntityManagerInterface;

class BookService
{
    private $manager;

    private $repository;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->repository = $manager->getRepository(Book::class);
    }

    public function getBooks()
    {
        $books = $this->repository->findAll();
        return $books;
    }

    public function getBook($id) {
        $book = $this->repository->find($id);
        if($book) {
            return [
                'name' => $book->getName(),
                'isbn' => $book->getIsbn(),
                'date' => $book->getDate()->format('d/m/Y'),
            ];
        }
        return [];
    }

    public function updateBook($id, array $values) {
        $book = $this->repository->find($id);

        if (!$book) {
            throw new \Exception();
        }

        $book->setName($values['name']);
        $book->setIsbn($values['isbn']);

        $date = new \DateTime($values['date']);
        $book->setDate($date);

        if($author = $this->manager->getRepository(Author::class)->find($values['author_id'])) {
            $book->setAuthor($author);
        }

        $this->manager->flush();
    }

    public function createBook(array $values) {
        $book = new book();
        $book->setName($values['name']);
        $book->setIsbn($values['isbn']);
        $date = new \DateTime($values['date']);
        $book->setDate($date);
        if($author = $this->manager->getRepository(Author::class)->find($values['author_id'])) {
            $book->setAuthor($author);
        }

        $this->manager->persist($book);
        $this->manager->flush();
    }
}