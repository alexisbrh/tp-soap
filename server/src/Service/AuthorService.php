<?php

namespace App\Service;

use App\Entity\Author;
use Doctrine\ORM\EntityManagerInterface;

class AuthorService
{
    private $manager;

    private $repository;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->repository = $manager->getRepository(Author::class);
    }

    public function getAuthors()
    {
        $authors = $this->repository->findAll();
        return $authors;
    }

    public function getAuthor($id) {
        $author = $this->repository->find($id);
        return $author;
    }

    public function updateAuthor($id, array $values) {
        $author = $this->getAuthor($id);

        if (!$author) {
            throw new \Exception();
        }

        $author->setName($values['name']);
        $author->setFirstname($values['firstname']);

        $this->manager->flush();
    }

    public function createAuthor(array $values) {
        $author = new author();
        $author->setName($values['name']);
        $author->setFirstname($values['firstname']);

        $this->manager->persist($author);
        $this->manager->flush();
    }
}