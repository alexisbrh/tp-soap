<?php

$client = new \SoapClient('http://localhost:8080/ws/book?wsdl', [
    'trace' => true,
    'cache_wsdl' => WSDL_CACHE_NONE
]);
$result = $client->__call('getBook', ['id' => '1']);
echo $result['name'];